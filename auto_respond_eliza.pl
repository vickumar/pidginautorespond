#!/usr/bin/perl

use strict;
use warnings;

use Net::DBus;
use Net::DBus::Reactor;
use Data::Dumper;
use Chatbot::Eliza;

exit main();

sub received_im_cb {
	my ($account, $sender, $message, $conversation) = @_;
	#print $sender." said: ".$message."\n";
	my $pidgin = Net::DBus->session->get_service('im.pidgin.purple.PurpleService')->get_object('/im/pidgin/purple/PurpleObject', 'im.pidgin.purple.PurpleInterface');
	my $im = $pidgin->PurpleConvIm ($conversation);
	#my $response = `fortune`;
	#my $megahal = AI::MegaHAL->new('Path' => '/home/vic/.megahal', 'Banner' => 0, 'Prompt' => 0, 'Wrap' => 0, 'AutoSave' => 0);
	#my $response = $megahal->initial_greeting();
	#$response = $megahal->do_reply($message);
	#$megahal->learn($message);
	my $mybot = new Chatbot::Eliza;
	my $response = $mybot->transform($message);
	$pidgin->PurpleConvImSend($im, "[auto] - ".$response);
}

sub main {			

	# Connecto to Pidgin through D-Bus
	my $pidgin = Net::DBus->session->get_service('im.pidgin.purple.PurpleService')->get_object('/im/pidgin/purple/PurpleObject', 'im.pidgin.purple.PurpleInterface');

	# We need the UI name for the calls to PurpleAccountGetEnabled()
	my $ui = $pidgin->PurpleCoreGetUi();

	# Loop throug all accounts available in Pidgin
	my $accounts = $pidgin->PurpleAccountsGetAll();
	foreach my $account (@{ $accounts }) {

		# For each account we need the protocol used, the name of the account and if
		# it's already enabled
		my $protocol = $pidgin->PurpleAccountGetProtocolName($account);
		my $name = $pidgin->PurpleAccountGetUsername($account);
		my $enabled = $pidgin->PurpleAccountGetEnabled($account, $ui);
						
		printf "%s\t\t%s\t\t%s\n", $protocol, $name, $enabled ? 'enabled' : 'disabled';
	}
	$pidgin->connect_to_signal("ReceivedImMsg", \&received_im_cb);
	my $reactor = Net::DBus::Reactor->main();

	#my $ticks = 0;
	#$reactor->add_timeout(1000, Net::DBus::Callback->new(method => sub {
		#if ($ticks++ == 1) {
      		#	$reactor->shutdown();
    		#}
	#}));
	$reactor->run();	
	return 0;
}
